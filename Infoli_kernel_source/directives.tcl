############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
set_directive_inline -off "ComputeOneCellTimeMux0"
set_directive_pipeline "IcNeighbors/IcNeighbors_label0"
set_directive_unroll -factor 12 "IcNeighbors/IcNeighbors_label0"
set_directive_loop_tripcount -min 96 -max 96 "IcNeighbors/IcNeighbors_label0"
set_directive_loop_tripcount -min 12 -max 12 "ComputeNetwork/ComputeNetwork_label0"
set_directive_loop_tripcount -min 12 -max 12 "ComputeNetwork/ComputeNetwork_label1"
set_directive_loop_tripcount -min 12 -max 12 "ComputeNetwork/ComputeNetwork_label2"
set_directive_loop_tripcount -min 12 -max 12 "ComputeNetwork/ComputeNetwork_label8"
set_directive_loop_tripcount -min 12 -max 12 "ComputeNetwork/ComputeNetwork_label9"
set_directive_loop_tripcount -min 12 -max 12 "ComputeOneCellTimeMux0/ComputeOneCellTimeMux0_label10"
set_directive_loop_tripcount -min 36 -max 36 "ComputeNetwork/ComputeNetwork_connectivity"
set_directive_resource -core AXI4Stream "ComputeNetwork" Connectivity_Matrix
set_directive_interface -mode ap_fifo -latency 0 "ComputeNetwork" iAppin
set_directive_resource -core AXI4Stream "ComputeNetwork" iAppin
set_directive_interface -mode ap_fifo -latency 0 "ComputeNetwork" cellOut
set_directive_resource -core AXI4Stream "ComputeNetwork" cellOut
set_directive_interface -mode ap_fifo -latency 0 "ComputeNetwork" IniArray
set_directive_interface -mode ap_vld -latency 0 "ComputeNetwork" ini
set_directive_interface -mode ap_stable -latency 0 "ComputeNetwork" N_Size
set_directive_interface -mode ap_stable -latency 0 "ComputeNetwork" Mux_Factor
set_directive_interface -mode ap_stable -latency 0 "ComputeNetwork" Conn_Matrix_Size
set_directive_interface -mode ap_vld -latency 0 "ComputeNetwork" new_matrix
set_directive_interface -mode ap_fifo -latency 0 "ComputeNetwork" Connectivity_Matrix
set_directive_resource -core AXI4Stream "ComputeNetwork" IniArray
set_directive_resource -core AXI4LiteS "ComputeNetwork" ini
set_directive_resource -core AXI4LiteS "ComputeNetwork" new_matrix
set_directive_resource -core AXI4LiteS "ComputeNetwork" N_Size
set_directive_resource -core AXI4LiteS "ComputeNetwork" Mux_Factor
set_directive_resource -core AXI4LiteS "ComputeNetwork" Conn_Matrix_Size
